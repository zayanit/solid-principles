<?php

interface LessonRepositoryInterface
{
    public function getAll(): array;
}

class FileLessonRepository implements LessonRepositoryInterface
{
    public function getAll(): array
    {
        //return through filesystem

        return [];
    }
}

class DBLessonRepository implements LessonRepositoryInterface
{
    public function getAll(): array
    {
        return Lesson::all()->toArray();
    }
}

function foo(LessonRepositoryInterface $lesson): array
{
    return $lesson->getAll();
}
