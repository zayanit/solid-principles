<?php

class VideoPlayer
{
    public function play($file)
    {
        //play the video
    }
}


// when A extends B we can use A in all places where B is used without any issues
class AviVideoPlayer extends VideoPlayer
{
    public function play($file)
    {
        if (pathinfo($file, PATHINFO_EXTENSION) != 'avi') {
            throw new Exception(); //violates the LSP
        }
    }
}
