<?php


namespace App\OpenClose;


interface ShapeInterface
{
    public function area();
}
