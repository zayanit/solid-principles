<?php


namespace App\OpenClose;


class Circle implements ShapeInterface
{
    public $radius;

    /**
     * Circle constructor.
     *
     * @param $radius
     */
    public function __construct($radius)
    {
        $this->radius = $radius;
    }

    public function area()
    {
        return $this->radius * $this->radius * pi();
    }
}
