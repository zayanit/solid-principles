<?php


namespace App\OpenClose;


interface PaymentMethodInterface
{
    public function acceptPayment($receipt);
}

class CashPaymentMethod implements PaymentMethodInterface
{

    public function acceptPayment($receipt)
    {
        // TODO: Implement acceptPayment() method.
    }
}

class BitCoinPaymentMethod implements PaymentMethodInterface
{

    public function acceptPayment($receipt)
    {
        // TODO: Implement acceptPayment() method.
    }
}

class Checkout
{
    public function begin(Receipt $receipt, PaymentMethodInterface $payment)
    {
        $payment->acceptPayment($receipt);
    }
}
