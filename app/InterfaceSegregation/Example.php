<?php

// A client should not be forced to implement an interface that it doesn't use

interface ManageableInterface
{
    public function beManaged();
}

interface WorkableInterface
{
    public function work();
}

interface SleepableInterface
{
    public function sleep();
}

class HumanWorker implements WorkableInterface, SleepableInterface, ManageableInterface
{

    public function work()
    {
        // TODO: Implement work() method.
    }

    public function sleep()
    {
        // TODO: Implement sleep() method.
    }

    public function beManaged()
    {
        $this->work();
        $this->sleep();
    }
}

class AndroidWorker implements WorkableInterface, ManageableInterface
{

    public function work()
    {
        // TODO: Implement work() method.
    }

    public function beManaged()
    {
        $this->work();
    }
}


class Captain
{
    public function manage(ManageableInterface $worker)
    {
        $worker->beManaged();
    }
}
