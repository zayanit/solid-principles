<?php


namespace App\InterfaceSegregation;


class HumanWorker implements WorkableInterface, SleepableInterface, ManageableInterface
{

    public function beManaged()
    {
        $this->work();
        $this->sleep();
    }

    public function work()
    {
        // TODO: Implement work() method.
    }

    public function sleep()
    {
        // TODO: Implement sleep() method.
    }
}
