<?php


namespace App\InterfaceSegregation;


interface ManageableInterface
{
    public function beManaged();
}
