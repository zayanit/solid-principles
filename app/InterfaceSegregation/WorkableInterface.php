<?php


namespace App\InterfaceSegregation;


interface WorkableInterface
{
    public function work();
}
