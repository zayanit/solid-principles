<?php


namespace App\InterfaceSegregation;


class AndroidWorker implements WorkableInterface, ManageableInterface
{

    public function beManaged()
    {
        $this->work();
    }

    public function work()
    {
        // TODO: Implement work() method.
    }
}
