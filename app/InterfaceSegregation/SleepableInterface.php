<?php


namespace App\InterfaceSegregation;


interface SleepableInterface
{
    public function sleep();
}
