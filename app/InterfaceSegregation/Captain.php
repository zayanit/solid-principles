<?php


namespace App\InterfaceSegregation;


class Captain
{
    public function manage(ManageableInterface $worker)
    {
        $worker->beManaged();
    }
}
