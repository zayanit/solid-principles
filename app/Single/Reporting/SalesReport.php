<?php


namespace App\Single\Reporting;

use App\Single\Repositories\SalesRepository;

class SalesReport
{
    private $repo;

    public function __construct(SalesRepository $repo)
    {
        $this->repo = $repo;
    }

    public function between($startDate, $endDate, SalesOutputInterface $formatter)
    {
        $sales = $this->repo->between($startDate, $endDate);

        return $formatter->output($sales);
    }
}
