<?php


namespace App\Single\Reporting;


interface SalesOutputInterface
{
    public function output($sales);
}
