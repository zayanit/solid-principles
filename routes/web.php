<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Single\Reporting\HtmlOutput;
use App\Single\Reporting\SalesReport;
use App\Single\Repositories\SalesRepository;
use Carbon\Carbon;

Route::get('/', function () {
    $report = new SalesReport(new SalesRepository());

    $begin = Carbon::now()->subDays(10);
    $end = Carbon::now();

    return $report->between($begin, $end, new HtmlOutput());
});
