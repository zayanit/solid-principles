<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Single\Sale;
use Illuminate\Support\Str;

$factory->define(Sale::class, function () {
    return [
        'description' => Str::random(10),
        'charge' => rand(100, 999)
    ];
});
